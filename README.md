## Install and bootstrap Loudml (optional)

```
docker pull loudml/loudml:latest-devel
docker run -ti -p 8077:8077 -v ./loudml/config.yml:/etc/loudml/config.yml:ro  -v ./loudml/store:/var/lib/loudml:rw loudml/loudml
```

## Install and bootstrap TICK stack

```
export IP=192.168.99.100 
export INFLUXDB_DATA=./influxdb
export KAPACITOR_DATA=./kapacitor/data
export KAPACITOR_CONF=./kapacitor/kapacitor.conf
export KAPACITOR_LOG=./kapacitor/log
export TELEGRAF_USER=ADMIN
export TELEGRAF_PASSWORD=egmMonitoring
export TELEGRAF_DB=telegraf
export GRAFANA_DATA=grafana-storage

docker-compose up
```